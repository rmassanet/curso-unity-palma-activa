﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour
{
	public GameObject playerPrefab;
	public Transform playerSpawnPoint;
	public Transform[] enemySpawnPoints;
	public GameObject enemyPrefab;
	public int killScore;
	public Text scoreText;
	public int scoreToFinish;

	private GameObject player;
	private int playerScore = 0;
	
	// Use this for initialization
	void Start ()
	{
		player = (GameObject) GameObject.Instantiate (playerPrefab, playerSpawnPoint.position, Quaternion.identity);
		player.GetComponent<InputController> ().gameController = gameObject;

		InvokeRepeating ("SpawnEnemy", 2.0f, 5.0f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void SpawnEnemy()
	{
		int idx = Random.Range (0, enemySpawnPoints.Length);
		Debug.Log ("idx: " + idx);
		Transform spawnPoint = enemySpawnPoints [idx];
		GameObject enemy = (GameObject) GameObject.Instantiate (enemyPrefab, spawnPoint.position, Quaternion.identity);
		enemy.GetComponent<Enemy2> ().player = player;
		enemy.GetComponent<Enemy2> ().gameController = gameObject;
	}

	void PlayerDied()
	{
		Debug.Log ("Player died!");
		Application.LoadLevel (0);
	}

	void EnemyDied()
	{
		Debug.Log ("Enemy died!");
		playerScore += killScore;
		if (playerScore >= scoreToFinish) {
			LevelComplete();
		}
		scoreText.text = playerScore.ToString ();
	}

	void LevelComplete()
	{
		Debug.Log ("Level complete!");
		Application.LoadLevel (1);
	}

	public void GoToMainMenu()
	{
		Application.LoadLevel ("MainMenu");
	}
}
