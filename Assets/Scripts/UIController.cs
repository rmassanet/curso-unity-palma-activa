﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class UIController : MonoBehaviour {

	private static UIController singleton;
	
	public Animator buttonsAnimator;

	private bool isAudioActive = true;

	// Use this for initialization
	void Start () {
		if (singleton == null) {
			singleton = this;
		} else {
			if(singleton != this)
			{
				Destroy(gameObject);
			}
		}

		DontDestroyOnLoad (gameObject);
	}

	void OnLevelWasLoaded(int level)
	{
		string currentLevel = Application.loadedLevelName;
		Debug.Log (currentLevel);
		if (currentLevel == "MainMenu") {
			GameObject uiCanvasObject = GameObject.FindGameObjectWithTag("UICanvas");
			buttonsAnimator = uiCanvasObject.GetComponent<Animator>();
		}
	}

	// Update is called once per frame
	void Update () {
	
	}

	public void NewGame()
	{
		Application.LoadLevel ("Level2");
	}

	public void ShowOptions()
	{
		buttonsAnimator.SetBool ("buttons", false);
	}

	public void HideOptions()
	{
		buttonsAnimator.SetBool ("buttons", true);
	}

	public void SetAudio(bool active)
	{
//		if (active) {
//			PlayerPrefs.SetInt ("audio", 1);
//		} else {
//			PlayerPrefs.SetInt ("audio", 0);
//		}

//		PlayerPrefs.SetInt ("audio", active ? 1 : 0);
		isAudioActive = active;
	}

	public void Exit()
	{
		Application.Quit ();
	}
}
