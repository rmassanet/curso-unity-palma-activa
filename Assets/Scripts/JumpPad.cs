﻿using UnityEngine;
using System.Collections;

public class JumpPad : MonoBehaviour
{
	public float jumpForce;

	void OnTriggerEnter2D(Collider2D other)
	{
		other.gameObject.rigidbody2D.AddForce(new Vector2(0, jumpForce));
	}
}
