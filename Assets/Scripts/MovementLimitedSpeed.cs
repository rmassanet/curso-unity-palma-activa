﻿using UnityEngine;
using System.Collections;

public class MovementLimitedSpeed : MonoBehaviour {

	public float maxSpeed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 position = transform.position;
		position.y -= maxSpeed * Time.deltaTime;
	}
}
