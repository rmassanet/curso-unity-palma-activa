﻿using UnityEngine;
using System.Collections;

public class InputController2 : MonoBehaviour
{
	public float speed;

	private Vector2 velocity = Vector2.zero;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		float h = Input.GetAxis ("Horizontal");
		velocity = new Vector2 (h * speed, 0);
		gameObject.transform.Translate (velocity * Time.deltaTime);
	}
}
