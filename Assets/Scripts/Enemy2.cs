﻿using UnityEngine;
using System.Collections;

public class Enemy2 : MonoBehaviour
{
	public float health;
	public GameObject player;
	public float speed;
	public ParticleSystem bloodParticle;
	public GameObject gameController;
	public float damage;

	public AudioClip[] deathClips;

	private float offset;
	private bool dying;
	private AudioSource audio;


	// Use this for initialization
	void Start () {
		audio = gameObject.GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		offset = player.transform.position.x - gameObject.transform.position.x;
	}

	void FixedUpdate()
	{
		if (dying) {
			return;
		}

		Vector2 velocity = gameObject.rigidbody2D.velocity;
		if (offset < 0) {
			velocity.x = -speed;
		}
		else if(offset > 0)
		{
			velocity.x = speed;
		}
		else
		{
			velocity.x = 0;
		}
		gameObject.rigidbody2D.velocity = velocity;
		
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "Player") {
			collision.gameObject.SendMessage("ApplyDamage", damage);
		}
	}

	void ApplyDamage(float damage)
	{
		if (dying) {
			return;
		}

		health -= damage;
		if (health <= 0) {
			Die ();
		}
	}

	void Die()
	{
		bloodParticle.Play ();
		PlayDeathAudio ();
		dying = true;
		StopMovement (); 
		Invoke ("DestroyEnemy", 1f);
		gameController.SendMessage ("EnemyDied");
	}

	void PlayDeathAudio()
	{
		int idx = Random.Range (0, deathClips.Length);
		audio.clip = deathClips [idx];
		audio.Play ();
	}

	void DestroyEnemy()
	{
		Destroy (gameObject);
	}

	void StopMovement()
	{
		Vector3 velocity = gameObject.rigidbody2D.velocity;
		velocity.x = 0;
		gameObject.rigidbody2D.velocity = velocity;
	}
}
