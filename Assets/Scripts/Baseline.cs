﻿using UnityEngine;
using System.Collections;

public class Baseline : MonoBehaviour
{
	public GameObject gameController;

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player") {
			// Game over
			gameController.SendMessage ("PlayerDied");
		} else {
			Destroy (other.gameObject);
		}
	}
}
