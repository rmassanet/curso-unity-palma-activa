﻿using UnityEngine;
using System.Collections;

public class Bomba : MonoBehaviour {
	public float damage;

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player")
		{
			PlayerHealth playerHealth = other.gameObject.GetComponent<PlayerHealth>();
			playerHealth.health -= damage;
			if(playerHealth.health <= 0)
			{
				Destroy(other.gameObject);
			}
		}
	}
}
