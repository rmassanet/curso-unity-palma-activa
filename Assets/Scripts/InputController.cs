﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour {
	public float speed;
	public float jumpForce;
	public Transform rocketSpawn;
	public GameObject rocketPrefab;
	public AudioClip footStepAudio;
	public float health;
	public GameObject gameController;

	private Vector3 scale;
	private bool jump;
	private Animator animator;
	private AudioSource audio;

	// Use this for initialization
	void Start () {
		scale = gameObject.transform.localScale;
		animator = gameObject.GetComponent<Animator> ();
		audio = gameObject.GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		jump = Input.GetButtonDown("Jump");
		if (Input.GetButtonDown ("Fire1")) {
			GameObject rocket = (GameObject) GameObject.Instantiate(rocketPrefab, rocketSpawn.position, Quaternion.identity);
			if(gameObject.transform.localScale.x > 0)
			{
				//rocket.GetComponent<Rocket2>().SetDirection(RocketDirection.RIGHT);
				rocket.SendMessage ("SetDirection", RocketDirection.RIGHT);
			}
			else
			{
				rocket.SendMessage ("SetDirection", RocketDirection.LEFT);
			}
		}
	}

	void FixedUpdate()
	{
		float h = Input.GetAxis ("Horizontal");
		Vector2 velocity = gameObject.rigidbody2D.velocity;
		velocity.x = h * speed;
		gameObject.rigidbody2D.velocity = velocity;
		if (h < 0) {
			gameObject.transform.localScale = new Vector3 (-scale.x, scale.y, scale.z); 
		} else if(h > 0){
			gameObject.transform.localScale = scale;
		}

		if (jump) {
			gameObject.rigidbody2D.AddForce(new Vector2(0, jumpForce));
			jump = false;
		}

		animator.SetFloat ("speed", Mathf.Abs(h));
	}

	public void OnFootStepEvent()
	{
		audio.clip = footStepAudio;
		audio.Play ();
	}

	public void ApplyDamage(float damage)
	{
		health -= damage;
		if (health <= 0) {
			Die ();
		}
	}

	public void Die()
	{
		gameController.SendMessage ("PlayerDied");
		Destroy (gameObject);
	}
}
