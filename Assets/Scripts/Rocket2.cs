﻿using UnityEngine;
using System.Collections;

public enum RocketDirection
{
	RIGHT,
	LEFT
}

public class Rocket2 : MonoBehaviour
{
	public float speed;
	public float damage;
	public float lifeTime;

	private Vector3 scale;

	void OnEnable()
	{
		scale = gameObject.transform.localScale;
	}

	void Start()
	{
		Invoke ("DestroyRocket", lifeTime);
	}

	void DestroyRocket()
	{
		Destroy (gameObject);
	}

	public void SetDirection(RocketDirection direction)
	{
		if (direction == RocketDirection.LEFT) {
			gameObject.rigidbody2D.velocity = new Vector2 (-speed, 0);
			gameObject.transform.localScale = new Vector3 (-scale.x, scale.y, scale.z);

		} else {
			gameObject.rigidbody2D.velocity = new Vector2 (speed, 0);
			gameObject.transform.localScale = scale;
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Enemy")
		{
			other.gameObject.SendMessage("ApplyDamage", damage);
		}
	}
}
